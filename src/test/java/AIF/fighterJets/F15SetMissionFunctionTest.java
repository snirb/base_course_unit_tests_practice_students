package AIF.fighterJets;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

import AIF.AIFUtil;
import AIF.Missions.*;
import org.junit.BeforeClass;
import org.junit.Test;


public class F15SetMissionFunctionTest {
    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass(){
        aifUtil = new AIFUtil();
    }


    @Test//Executable Mission -> IntelligenceMission
    public void testF15ExecutableMission() throws MissionTypeException{
        try{
            aifUtil.getAerialVehiclesHashMap().get("F15").setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();

        }
    }

    @Test//Executable Mission -> AttackMission
    public void testF15UnexecutableMission() throws MissionTypeException{
        try{
            aifUtil.getAerialVehiclesHashMap().get("F15").setMission(aifUtil.getAllMissions().get("bda"));
            fail();
        }catch (MissionTypeException missionTypeException){
            assertTrue(true);
        }
    }
}

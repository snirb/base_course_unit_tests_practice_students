package AIF.fighterJets;

import AIF.AIFUtil;
import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class F16SetMissionFunctionTest {

    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass(){
        aifUtil = new AIFUtil();
    }


    @Test//Executable Mission -> IntelligenceMission
    public void testF16ExecutableMission() throws MissionTypeException {
        try{
            aifUtil.getAerialVehiclesHashMap().get("F16").setMission(aifUtil.getAllMissions().get("attack"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the current aerial vehicle.
            fail();

        }
    }

    @Test//Executable Mission -> AttackMission
    public void testF16UnexecutableMission() throws MissionTypeException{
        try{
            aifUtil.getAerialVehiclesHashMap().get("F16").setMission(aifUtil.getAllMissions().get("intelligence"));
//            fail();
        }catch (MissionTypeException missionTypeException){
            assertTrue(true);
        }
    }
}

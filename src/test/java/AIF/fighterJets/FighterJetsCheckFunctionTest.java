package AIF.fighterJets;

//incomplete ...


import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import AIF.AIFUtil;
import AIF.AerialVehicles.AerialVehicle;
import org.junit.BeforeClass;
import org.junit.Test;



public class FighterJetsCheckFunctionTest {

    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        aifUtil = new AIFUtil();
    }

    @Test
    public void testCheckUniqueMinusCaseFighterJets(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F16"),-1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -1 should throws an exception ? or reset to zero ?.", aifUtil.getAerialVehiclesHashMap().get("F16").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckUniqueZeroCaseFighterJets(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F16"),0);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 f16.check().", aifUtil.getAerialVehiclesHashMap().get("F16").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsPositiveLessThanLimit(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F16"),1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 1 should be reset to 1 f16.check().", aifUtil.getAerialVehiclesHashMap().get("F16").getHoursOfFlightSinceLastRepair() == 1);
    }


    @Test
    public void testCheckLimitsFighterJetsEqualsLimit(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F16"),250);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 f16.check().", aifUtil.getAerialVehiclesHashMap().get("F16").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test//299
    public void testCheckFighterJetsMoreThanLimit(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("F16"),299);//1444 - arbitrary
        assertTrue("failure - hoursOfFlightSinceLastRepair = 144 shouldn't be reset to 0 after f16.check().", aifUtil.getAerialVehiclesHashMap().get("F16").getHoursOfFlightSinceLastRepair() == 0);
    }


}

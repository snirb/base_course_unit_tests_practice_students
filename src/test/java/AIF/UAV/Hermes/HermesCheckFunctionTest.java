package AIF.UAV.Hermes;

//incomplete ...

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.not;

import AIF.AIFUtil;
import AIF.AerialVehicles.FighterJets.F15;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;


public class HermesCheckFunctionTest {

    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        aifUtil = new AIFUtil();
    }

    @Test
    public void testCheckUniqueMinusCaseFighterJets(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Zik"),-1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = -1 should throws an exception ? or reset to zero ?.", aifUtil.getAerialVehiclesHashMap().get("Zik").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckUniqueZeroCaseFighterJets(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Zik"),0);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 Zik.check().", aifUtil.getAerialVehiclesHashMap().get("Zik").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test
    public void testCheckLimitsFighterJetsPositiveLessThanLimit(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Zik"),1);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 1 should be reset to 1 Zik.check().", aifUtil.getAerialVehiclesHashMap().get("Zik").getHoursOfFlightSinceLastRepair() == 1);
    }


    @Test
    public void testCheckLimitsFighterJetsEqualsLimit(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Zik"),100);
        assertTrue("failure - hoursOfFlightSinceLastRepair = 250 should be reset to 0 Zik.check().", aifUtil.getAerialVehiclesHashMap().get("Zik").getHoursOfFlightSinceLastRepair() == 0);
    }

    @Test//299
    public void testCheckFighterJetsMoreThanLimit(){
        aifUtil.setHoursAndCheck(aifUtil.getAerialVehiclesHashMap().get("Zik"),299);//1444 - arbitrary
        assertTrue("failure - hoursOfFlightSinceLastRepair = 144 shouldn't be reset to 0 after Zik.check().", aifUtil.getAerialVehiclesHashMap().get("Zik").getHoursOfFlightSinceLastRepair() == 0);
    }


}

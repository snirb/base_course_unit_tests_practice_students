package AIF.AerialVehicles.UAVs.Haron;

import AIF.AerialVehicles.AerialAttackVehicle;
import AIF.AerialVehicles.UAVs.UAV;
import AIF.Missions.AttackMission;
import AIF.Missions.Mission;

public abstract class Haron extends UAV implements AerialAttackVehicle {
    private static final int MAX_HOURS_AFTER_REPAIR = 150;
    int amountOfMissile;
    String missileModel;

    public Haron(int amountOfMissile, String missileModel, String sensorType, String pilotName, Mission mission, int hoursOfFlightSinceLastRepair, Boolean readyToFly) {
        super(sensorType, pilotName, mission, hoursOfFlightSinceLastRepair, readyToFly);
        this.amountOfMissile = amountOfMissile;
        this.missileModel = missileModel;
    }

    @Override
    public String attack() {
        String message = this.getPilotName() + ": " + this.getClass().getSimpleName() +
                " Attacking " + ((AttackMission)this.getMission()).getTarget() +
                " with: " + this.getMissileModel() + "X" + this.getAmountOfMissile();
        System.out.println(message);
        return message;
    }

    @Override
    public void check() {
        if (this.getHoursOfFlightSinceLastRepair() >= MAX_HOURS_AFTER_REPAIR){
            this.repair();
        }
    }

    public int getAmountOfMissile() {
        return amountOfMissile;
    }

    public String getMissileModel() {
        return this.missileModel;
    }


}

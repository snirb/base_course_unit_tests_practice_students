package AIF.AerialVehicles.UAVs.Hermes;

import AIF.AerialVehicles.AerialBdaVehicle;
import AIF.AerialVehicles.UAVs.UAV;
import AIF.Missions.BdaMission;
import AIF.Missions.Mission;

public abstract class Hermes extends UAV implements AerialBdaVehicle {
    private static final int MAX_HOURS_AFTER_REPAIR = 100;
    String cameraType;

    public Hermes(String cameraType, String sensorType, String pilotName, Mission mission, int hoursOfFlightSinceLastRepair, Boolean readyToFly) {
        super(sensorType, pilotName, mission, hoursOfFlightSinceLastRepair, readyToFly);
        this.cameraType = cameraType;
    }

    @Override
    public String preformBda() {
        String message = this.getPilotName() + ": " + this.getClass().getSimpleName() +
                " taking pictures of " + ((BdaMission)this.getMission()).getObjective() +
                " with: " + this.getCameraType() + " camera";
        System.out.println(message);
        return message;
    }

    @Override
    public void check() {
        if (this.getHoursOfFlightSinceLastRepair() >= MAX_HOURS_AFTER_REPAIR){
            this.repair();
        }
    }

    public String getCameraType() {
        return this.cameraType;
    }
}
